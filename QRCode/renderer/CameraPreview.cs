﻿using System;
using QRCode.Enums;
using Xamarin.Forms;

namespace QRCode.renderer
{
    public class CameraPreview : View
    {
        public static readonly BindableProperty CameraProperty = BindableProperty.Create(propertyName: "Camera",
                                                                                         returnType: typeof(CameraOptions),
                                                                                         declaringType: typeof(CameraPreview),
                                                                                         defaultValue: CameraOptions.Rear);

        public CameraOptions Camera
        {
            get { return (CameraOptions)GetValue(CameraProperty); }
            set { SetValue(CameraProperty, value); }
        }

        public event EventHandler<string> resultScanQRCode;

        public void ResultScanQRCode(string code)
        {
            if(resultScanQRCode != null) 
            {
                resultScanQRCode(this, code);
            }
        }

    }
}
