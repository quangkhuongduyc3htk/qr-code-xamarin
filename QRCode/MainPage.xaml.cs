﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Plugin.Permissions.Abstractions;
using Plugin.Permissions;
using System.Diagnostics;

namespace QRCode
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);


            Device.BeginInvokeOnMainThread(async () =>
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                Debug.WriteLine($"Status: {status}");

                if (status == PermissionStatus.Granted)
                {

                    //cameraView.Camera = XLabs.Platform.Services.Media.CameraDevice.Rear;
                }
                else
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                    Debug.WriteLine($"results: {results}");
                }
            });
        }

        void Handle_resultScanQRCode(object sender, string code)
        {
            Debug.WriteLine($"Decode QR: {code}");
            Device.BeginInvokeOnMainThread(() =>
            {
                renderCodeQR.Content = code;
                lblCode.Text = $"Code: {code}";
            });
        }
    }
}
