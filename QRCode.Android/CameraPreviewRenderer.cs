﻿using System;
using QRCode.Droid;
using QRCode.Droid.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using Android.Hardware;

[assembly: ExportRenderer(typeof(QRCode.renderer.CameraPreview), typeof(CameraPreviewRenderer))]
namespace QRCode.Droid
{
    public class CameraPreviewRenderer : ViewRenderer<QRCode.renderer.CameraPreview, CustomRenderer.CameraPreview>, ICameraPreviewDelege
    {
        CameraPreview cameraPreview;
        Context context;
        public CameraPreviewRenderer(Context context) : base(context)
        {
            this.context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<QRCode.renderer.CameraPreview> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                cameraPreview = new CameraPreview(Context);
                cameraPreview.iCameraPreview = this;
                SetNativeControl(cameraPreview);
            }

            if (e.OldElement != null)
            {
                // Unsubscribe
                cameraPreview.Click -= OnCameraPreviewClicked;
            }
            if (e.NewElement != null)
            {
                //Control.Preview = Camera.Open((int)e.NewElement.Camera);

                // Subscribe
                cameraPreview.Click += OnCameraPreviewClicked;
            }
        }

        void OnCameraPreviewClicked(object sender, EventArgs e)
        {
            if (cameraPreview.IsPreviewing)
            {
                //cameraPreview.Preview.StopPreview();
                cameraPreview.StopCamera();
                cameraPreview.IsPreviewing = false;
            }
            else
            {
                //cameraPreview.Preview.StartPreview();
                cameraPreview.StartCamera();
                cameraPreview.IsPreviewing = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //Control.Preview.Release();
                Control.ReleaseCamera();
            }
            base.Dispose(disposing);
        }
        string codeOld = "";
        public void resultQRCode(string code)
        {
            if (codeOld.Equals(code)) return;
            codeOld = code;
            Element.ResultScanQRCode(code);
        }
    }
}
