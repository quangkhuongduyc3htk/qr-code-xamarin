﻿using System;
using System.Diagnostics;
using System.Linq;
using AVFoundation;
using CoreFoundation;
using CoreGraphics;
using Foundation;
using QRCode.Enums;
using UIKit;

namespace QRCode.iOS
{
    public interface IUICameraPreviewDelege
    {
        void resultScanQRCode(string code);
        void ImageTake(UIImage image);
    }

    public class UICameraPreview : UIView, IAVCaptureMetadataOutputObjectsDelegate
    {
        public UICameraPreview()
        {
        }

        AVCaptureVideoPreviewLayer previewLayer;
        CameraOptions cameraOptions;

        public IUICameraPreviewDelege delege;

        public event EventHandler<EventArgs> Tapped;

        public AVCaptureSession CaptureSession { get; private set; }

        public bool IsPreviewing { get; set; }

        AVCaptureStillImageOutput aVCapture = new AVCaptureStillImageOutput();

        public UICameraPreview(CameraOptions options)
        {
            cameraOptions = options;
            IsPreviewing = false;
            Initialize();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            previewLayer.Frame = rect;
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            OnTapped();
        }

        protected virtual void OnTapped()
        {
            var eventHandler = Tapped;
            if (eventHandler != null)
            {
                eventHandler(this, new EventArgs());
            }
        }

        void Initialize()
        {
            CaptureSession = new AVCaptureSession();
            previewLayer = new AVCaptureVideoPreviewLayer(CaptureSession)
            {
                Frame = Bounds,
                VideoGravity = AVLayerVideoGravity.ResizeAspectFill
            };

            var videoDevices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);
            var cameraPosition = (cameraOptions == CameraOptions.Front) ? AVCaptureDevicePosition.Front : AVCaptureDevicePosition.Back;
            var device = videoDevices.FirstOrDefault(d => d.Position == cameraPosition);

            if (device == null)
            {
                return;
            }

            NSError error;
            var input = new AVCaptureDeviceInput(device, out error);
            CaptureSession.AddInput(input);
            Layer.AddSublayer(previewLayer);
            this.AddGestureRecognizer(new UIGestureRecognizer(HandleAction));

            var output = new AVCaptureStillImageOutput();
            var dict = new NSMutableDictionary();
            dict[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
            CaptureSession.AddOutput(output);


            var captureMetadataOutput = new AVCaptureMetadataOutput();
            CaptureSession?.AddOutput(captureMetadataOutput);
            captureMetadataOutput.SetDelegate(this, DispatchQueue.MainQueue);
            captureMetadataOutput.MetadataObjectTypes = AVMetadataObjectType.QRCode;


            CaptureSession.StartRunning();
            IsPreviewing = true;

            //aVCapture.OutputSettings = dict;

            if(CaptureSession.CanAddOutput(aVCapture))
            {
                CaptureSession.AddOutput(aVCapture);
            }

        }


        public void stopCamera()
        {
            CaptureSession.StopRunning();
            IsPreviewing = false;
        }

        public void startCamera()
        {
            CaptureSession.StartRunning();
            IsPreviewing = true;
        }

        public void HandleAction()
        {

            var videoConnection = aVCapture.ConnectionFromMediaType(AVMediaType.Video);
            if(videoConnection != null)
            {
                aVCapture.CaptureStillImageAsynchronously(videoConnection, (imageDataSampleBuffer, error) =>
                {
                    var imageData = AVCaptureStillImageOutput.JpegStillToNSData(imageDataSampleBuffer);
                    var img = new UIImage(imageData);
                    delege?.ImageTake(img);
                });
            }

        }


        [Export("captureOutput:didOutputMetadataObjects:fromConnection:")]
        public void DidOutputMetadataObjects(AVCaptureMetadataOutput captureOutput, AVMetadataObject[] metadataObjects, AVCaptureConnection connection)
        {
            if (metadataObjects.Length == 0) return;
            var readableCode = metadataObjects[0] as AVMetadataMachineReadableCodeObject;
            if (readableCode == null) return;
            var code = readableCode.StringValue;
            if (code == null) return;
            delege?.resultScanQRCode(code);
        }

    }
}
