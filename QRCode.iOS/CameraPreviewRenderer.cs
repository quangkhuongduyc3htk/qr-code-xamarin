﻿
using System;
using System.Diagnostics;
using QRCode.iOS;
using QRCode.renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CameraPreview), typeof(CameraPreviewRenderer))]
namespace QRCode.iOS
{
    public class CameraPreviewRenderer : ViewRenderer<CameraPreview, UICameraPreview>, IUICameraPreviewDelege
    {
        UICameraPreview uiCameraPreview;

        protected override void OnElementChanged(ElementChangedEventArgs<CameraPreview> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                uiCameraPreview = new UICameraPreview(e.NewElement.Camera);
                uiCameraPreview.delege = this;
                SetNativeControl(uiCameraPreview);
            }
            if (e.OldElement != null)
            {
                // Unsubscribe
                uiCameraPreview.Tapped -= OnCameraPreviewTapped;
            }
            if (e.NewElement != null)
            {
                // Subscribe
                uiCameraPreview.Tapped += OnCameraPreviewTapped;
            }
        }

        void OnCameraPreviewTapped(object sender, EventArgs e)
        {
            //if (uiCameraPreview.IsPreviewing)
            //{
            //    uiCameraPreview.stopCamera();
            //}
            //else
            //{
            //    uiCameraPreview.startCamera();
            //}
            uiCameraPreview.HandleAction();


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Control.CaptureSession.Dispose();
                Control.Dispose();
            }
            base.Dispose(disposing);
        }
        string codeOld = "";
        void IUICameraPreviewDelege.resultScanQRCode(string code)
        {
            if (codeOld.Equals(code)) return;
            codeOld = code;
            Element.ResultScanQRCode(code);
        }

        public void ImageTake(UIImage image)
        {

        }
    }
}
